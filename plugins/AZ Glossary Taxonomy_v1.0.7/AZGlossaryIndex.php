<?php 
/*
Plugin Name: AZ Glossary Taxonomy Index
Plugin URI: http://azglossary.ryhab.com/
Description: Creates an A to Z glossary index for your categories or custom taxonomies and Tags
Version: 1.07
Author: Ivano Di Biasi & Giuseppe Liguori
Author URI: http://www.seocube.it
*/


  global $glossaryindexoptions_1_metabox;
	global $td;
	$td="AZGlossaryIndex"; //textdomain  
  

function register_azglossary_posttype() {
		global $glossaryindexoptions_1_metabox;
		global $td;
		
		$labels = array(
			'name' 				=> _x( 'AZ Glossaries', 'post type general name', $td ),
			'singular_name'		=> _x( 'AZ Glossary', 'post type singular name', $td ),
			'add_new' 			=> __( 'Add New', $td ),
			'add_new_item' 		=> __( 'Add New Glossary', $td ),
			'edit_item' 		=> __( 'Edit Glossary', $td ),
			'new_item' 			=> __( 'New Glossary', $td ),
			'view_item' 		=> __( 'View Glossary', $td ),
			'search_items' 		=> __( 'Search Glossary', $td ),
			'not_found' 		=> __( 'Glossary not found', $td ),
			'not_found_in_trash'=> __( 'AZ Glossary', $td ),
			'parent_item_colon' => __( 'AZ Glossary', $td ),
			'menu_name'			=> __( 'AZ Glossaries', $td )
		);
		
		$taxonomies = array();

		$supports = array('title');
		$plugin_dir_url =plugin_dir_url( __FILE__ );

		$post_type_args = array(
			'labels' 			=> $labels,
			'singular_label' 	=> __('AZ Glossary', $td),
			'public' 			=> false,
			'show_ui' 			=> true,
			'publicly_queryable'=> true,
			'query_var'			=> true,
			'exclude_from_search'=> false,
			'show_in_nav_menus'	=> true,
			'capability_type' 	=> 'post',
			'has_archive' 		=> false,
			'hierarchical' 		=> false,
			'rewrite' 			=> array('slug' => 'azglossary', 'with_front' => false ),
			'supports' 			=> $supports,
			'menu_position' 	=> 5,
			'menu_icon' 		=> $plugin_dir_url . 'glossary1.jpg',
			'taxonomies'		=> $taxonomies
		 );
		 register_post_type('azglossary',$post_type_args);
		 //Read all registered taxonomies
	$args = array(
	  'public'   => true,
	  '_builtin' => false
	  
	); 
	$output = 'names';
	$rtaxonomies = get_taxonomies( $args, $output); 
  $rtaxonomies[]="category";
  $rtaxonomies[]="post_tag";
  
  $csspath=  dirname(__FILE__) . "/styles";
  
	if ($handle = opendir($csspath)) {
	    while (false !== ($entry = readdir($handle))) {
	        if (strpos($entry,".css")>0) {
	            $cssarray[]=array(str_replace(".css","",strtolower($entry)),$entry);
	        }
	    }
	    closedir($handle);
	}

  
  
	$glossaryindexoptions_1_metabox = array(
	'id' => 'glossaryindexoptions',
	'title' => __('Glossary Index Options', $td),
	'page' => array('azglossary'),
	'context' => 'advanced',
	'priority' => 'high',
	'fields' => array(

				array(
					'name' 			=> __('Source Taxonomy', $td),
					'desc' 			=> __('Select the source taxonomy (category or custom taxonomy) to get the desired items', $td),
					'id' 				=> 'gti_sourcetaxonomy',
					'class' 			=> 'gti_sourcetaxonomy',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => $rtaxonomies,
					'max' 			=> 0				),
			
			array(
					'name' 			=> __('Specific Terms id', $td),
					'desc' 			=> __('Insert the specific category or term. The plugin display posts inside. Insert terms id comma separated ex.: 4,84,12. Anyway you must select a source taxonomy (mandatory field)', $td),
					'id' 				=> 'gti_sourceitem',
					'class' 			=> 'gti_sourceitem',
					'type' 			=> 'text',
					'rich_editor' 	=> 0,
					'width'   => '120px',
					'max' 			=> 0				),
			
				array(
					'name' 			=> __('Terms with no posts', $td),
					'desc' 			=> __('Decide whether you want to show terms with zero posts', $td),
					'id' 				=> 'gti_hideempty',
					'class' 			=> 'gti_hideempty',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Show empty', $td),__('Hide empty', $td)),
					'max' 			=> 0				),
			
				array(
					'name' 			=> __('Show numbers', $td),
					'desc' 			=> __('Decide whether you want to show terms starting with numbers', $td),
					'id' 				=> 'gti_shownumbers',
					'class' 			=> 'gti_shownumbers',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Show numbers', $td),__('Hide numbers', $td)),
					'max' 			=> 0				),
			
				array(
					'name' 			=> __('Show symbols', $td),
					'desc' 			=> __('Decide whether you want to show terms starting with symbols', $td),
					'id' 				=> 'gti_showsymbols',
					'class' 			=> 'gti_showsymbols',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Show symbols', $td),__('Hide symbols', $td)),
					'max' 			=> 0				),
			
			array(
					'name' 			=> __('Show posts count', $td),
					'desc' 			=> __('Decide whether you want to show the post count for each term. Ex.: Travels (10)', $td),
					'id' 				=> 'gti_showcount',
					'class' 			=> 'gti_showcount',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Show post count', $td),__('Hide post count', $td)),
					'max' 			=> 0				),
				array(
					'name' 			=> __('Display Glossary Title', $td),
					'desc' 			=> __('Decide whether you want to show the Glossary Title', $td),
					'id' 				=> 'gti_showtitle',
					'class' 			=> 'gti_showtitle',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Show Title', $td),__('Hide Title', $td)),
					'max' 			=> 0				),
				
				array(
					'name' 			=> __('Text before terms', $td),
					'desc' 			=> __('A text added before the term name, ex.: If term is INGREDIENT you could add "recipes with " before INGREDIENT to obtain "recipes with pasta" and so on...', $td),
					'id' 				=> 'gti_textbefore',
					'class' 			=> 'gti_textbefore',
					'type' 			=> 'text',
					'rich_editor' 	=> 0,
					'max' 			=> 0,
					'width'   => '95%'),
				array(
					'name' 			=> __('Text after terms', $td),
					'desc' 			=> __('A text added after the term name, ex.: If term is SERVICE NAME you could add " in New York" after SERVICE NAME to obtain "car rental in New York" and so on...', $td),
					'id' 				=> 'gti_textafter',
					'class' 			=> 'gti_textafter',
					'type' 			=> 'text',
					'rich_editor' 	=> 0,
					'max' 			=> 0,
					'width'   => '95%'),
			
				array(
					'name' 			=> __('Index Panel Behaviour', $td),
					'desc' 			=> __('Define the Index Panel behaviour. It could be a filtering panel or a navigation panel. In Navigation mode you will see all the terms of each letter, in filter mode you will see the terms of one letter a time.', $td),
					'id' 				=> 'gti_indexpanelbehaviour',
					'class' 			=> 'gti_indexpanelbehaviour',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Navigation', $td),__('Filtering', $td)),
					'max' 			=> 0				),
			
				array(
					'name' 			=> __('Index Panel Position', $td),
					'desc' 			=> __('Where you want the panel to be shown', $td),
					'id' 				=> 'gti_indexpanelposition',
					'class' 			=> 'gti_indexpanelposition',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => array(__('Top', $td),__('Bottom', $td),__('Top and bottom', $td)),
					'max' 			=> 0				),
			
				array(
					'name' 			=> __('Column Count', $td),
					'desc' 			=> __('Number of columns for each row', $td),
					'id' 				=> 'gti_columncount',
					'class' 			=> 'gti_columncount',
					'type' 			=> 'text',
					'rich_editor' 	=> 0,
					'max' 			=> 0,
					'std'     => 3,
					'width'   => '60px'),
			
				array(
					'name' 			=> __('Theme', $td),
					'desc' 			=> __('Select the Glossary look and feel<br/>If you would customize your glossary, you need to create a new css file named as you like and save it in the plugin "styles" folder.', $td),
					'id' 				=> 'gti_theme',
					'class' 			=> 'gti_theme',
					'type' 			=> 'select',
					'rich_editor' 	=> 0,
					'options' => $cssarray,
					'max' 			=> 0				)
				)
);
	}
	add_action('init', 'register_azglossary_posttype',9999);
	


add_action('admin_menu', 'gti_add_glossaryindexoptions_1_meta_box');



function gti_add_glossaryindexoptions_1_meta_box() {
	global $glossaryindexoptions_1_metabox;

	foreach($glossaryindexoptions_1_metabox['page'] as $page) {
		add_meta_box($glossaryindexoptions_1_metabox['id'], $glossaryindexoptions_1_metabox['title'], 'gti_show_glossaryindexoptions_1_box', $page, 'advanced', 'high', $glossaryindexoptions_1_metabox);
	}
}

// function to show meta boxes
function gti_show_glossaryindexoptions_1_box()	{
	global $post;
	global $glossaryindexoptions_1_metabox;
	global $gti_prefix;
	global $wp_version;
	global $td;

	// Use nonce for verification
	echo '<input type="hidden" name="gti_glossaryindexoptions_1_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

	echo '<table class="form-table">';

	foreach ($glossaryindexoptions_1_metabox['fields'] as $field) {
		// get current post meta data

		$meta = get_post_meta($post->ID, $field['id'], true);

		echo '<tr>',
				'<th style="width:150px;font-weight:bold;"><label for="', $field['id'], '">', stripslashes($field['name']), '</label></th>',
				'<td style="font-style:italic;" class="gti_field_type_' . str_replace(' ', '_', $field['type']) . '">';
		switch ($field['type']) {
			case 'text':
				$f_size=$field['width'];
				if ($f_size=="") $f_size="97%";
				echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" style="width:'.$f_size.'" /><br/>', '', stripslashes($field['desc']);
				break;
			case 'select':
				echo '<select name="', $field['id'], '" id="', $field['id'], '">';
				foreach ($field['options'] as $option) {
					if (is_array($option)) {
						echo '<option value="' . $option[1] . '"', $meta == $option[1] ? ' selected="selected"' : '', '>', ucfirst($option[0]), '</option>';
					} else {
						echo '<option value="' . $option . '"', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
					}
				}
				echo '</select><br/>', '', stripslashes($field['desc']);
				break;
		}
		echo     '<td>',
			'</tr>';
	}
	//showglossary
	if ($_GET["action"]=="edit") {
		if (is_numeric($_GET["post"])){
			echo '<tr><th style="width:150px;font-weight:bold;"><label for="theshortcode">'.__('Use this shortcode', $td).'</label></th>',
					'<td style="font-style:italic;">[showglossary gid="'. $_GET["post"] .'"] '.__('in a page or article or use', $td).' &lt;?php do_shortcode(\'[showglossary gid="'. $_GET["post"] .'"]\'); ?&gt; '.__('into your wordpress theme.', $td).'</td></tr>';
		}		
	}
 echo '</table>';
}

// Save data from meta box
add_action('save_post', 'gti_glossaryindexoptions_1_save');
function gti_glossaryindexoptions_1_save($post_id) {
	global $post;
	global $glossaryindexoptions_1_metabox;
	// verify nonce
	if ( ! isset( $_POST['gti_glossaryindexoptions_1_meta_box_nonce'] ) || ! wp_verify_nonce($_POST['gti_glossaryindexoptions_1_meta_box_nonce'], basename(__FILE__))) {
		return $post_id;
	}

	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
		return $post_id;
	}

	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id)) {
			return $post_id;
		}
	} elseif (!current_user_can('edit_post', $post_id)) {
		return $post_id;
	}

	foreach ($glossaryindexoptions_1_metabox['fields'] as $field) {

		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			if($field['type'] == 'date') {
				$new = gti_format_date($new);
				update_post_meta($post_id, $field['id'], $new);
			} else {
				if(is_string($new)) {
					$new = $new;
				}
				update_post_meta($post_id, $field['id'], $new);


			}
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	}

}




//----------------------------------FRONT------------------------------------


function uniord($u) {
    // i just copied this function fron the php.net comments, but it should work fine!
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
}


function is_unicodeletter($string)
{
	$pos=uniord($string);
	//echo $pos . $string . "<br/>";
	if (($pos >= 1536 && $pos <= 1791) || ($pos >= 97 && $pos <= 122) || ($pos >= 192 && $pos <= 255) || ($pos >= 880 && $pos <= 1279) ) {
		return true;
	} else {
		return false;
	}
}

function alphanum($string) {
	if (ctype_alnum($string) || is_unicodeletter($string)) return true;
	
	}
	
function rmBOM($string) { 
    if(substr($string, 0,3) == pack("CCC",0xef,0xbb,0xbf)) { 
        $string=substr($string, 3); 
    } 
    return $string; 
}
	
function getGlossaryHtmlBySourceTax($gid,$gender){
	global $td;
	$html="";
	$sourcetax= get_post_meta($gid,"gti_sourcetaxonomy",true);
	$sourceitem = get_post_meta($gid,"gti_sourceitem",true);
	$hideempty=get_post_meta($gid,"gti_hideempty",true);
	$shownumbers=get_post_meta($gid,"gti_shownumbers",true);
	$showsymbols=get_post_meta($gid,"gti_showsymbols",true);
	$showcount=get_post_meta($gid,"gti_showcount",true);
	$showtitle=get_post_meta($gid,"gti_showtitle",true);
	$textbefore= get_post_meta($gid,"gti_textbefore",true);
	$textafter= get_post_meta($gid,"gti_textafter",true);
	$behaviour= get_post_meta($gid,"gti_indexpanelbehaviour",true);
	$position= get_post_meta($gid,"gti_indexpanelposition",true);
	$columncount= get_post_meta($gid,"gti_columncount",true);
	$theme= get_post_meta($gid,"gti_theme",true);
	$args="";
	if ($hideempty==__('Hide empty',$td)) {
		$args="hide_empty=1";
	} else {
		$args="hide_empty=0";
	}

	$colsize="33%";
	if (is_numeric($columncount)) {
				if($columncount>0){
						$colsize=floor(100/$columncount) . "%";
				}
	}	
	 $args = "orderby=name&order=asc&".$args;
	 $terms = get_terms($sourcetax,$args);
	 if (count($terms)==0) return __("No terms found!",$td);
   foreach ( $terms as $term ) {
   mb_internal_encoding(mb_detect_encoding($term->name));

	 $firstchar = trim(mb_substr((rmBOM($term->name)),0,1));
		
//echo $firstchar . " ----- " . $term->name . " @@@@@ " . uniord($firstchar) . "#" . alphanum($firstchar) . "#" . "<br/>";

   		if (is_numeric($firstchar) && 1==2) {
   			if($shownumbers==__("Show numbers", $td)){
				 	//$letters['#'][]=$term;
				}
			} elseif (!alphanum($firstchar)) {
					if ($showsymbols==__("Show symbols", $td)) {
				 		$letters['@'][]=$term;
				 	}	
			} else {
				 $letters[strtoupper($firstchar)][]=$term;
			}
   }
	 $htmllist="";
	 $htmlindex='<div id="gti_letterindex">';
	 if ($behaviour==__("Filtering", $td)){
			$htmlindex.='<li><a href="#gti_all">'.__('All', $td).'</a></li>';			 			
	 }
	 foreach ($letters as $letter=>$lettervalue) {
	 		$ident=str_replace("#","sym",$letter);
	 		$ident=str_replace("@","num",$ident);
	 	  if ($behaviour==__("Navigation", $td)) {
				$htmlindex.='<li><a href="#gti_'.$ident.'">'.$letter.'</a></li>';	
			} else {
				$htmlindex.='<li><a id="gti_sel_'.$ident.'" href="#">'.$letter.'</a></li>';	
			}
			if ($showcount==__("Show post count", $td)) {
				//echo '$lettervalue as $theterm';
				$theterm = $lettervalue[0];
					
					if($gender){
						$htmllist.='<a href="'.get_term_link($theterm, $sourcetax).'?gender='.$gender.'">'.$textbefore.$theterm->name.'</a>';
					}else{
						$htmllist.='<a href="'.get_term_link($theterm, $sourcetax).'">'.$textbefore.$theterm->name.'</a>';
					}
			}else {
				foreach ($lettervalue as $theterm) {
					$htmllist.='<li style="width:'.$colsize.';"><a href="'.get_term_link($theterm, $sourcetax).'">'.$textbefore.$theterm->name.$textafter.'</a></li>';
	 			}
			}
	 	}
		if ($showtitle==__("Show Title", $td)) {
			$html.='<h2>'.get_the_title($gid).'</h2>';
		}
		$htmlindex = '';
		$html.='<div id="gticontainer">';
		if ($position==__("Top", $td) || $position==__("Top and bottom", $td)) {
			$html.=$htmlindex;
		}
		//$htmllist = '';
		$html.='<div class="alphabet-nav super-alphabet-nav">
        <h3>Search baby names by letter</h3>
        <div class="alphabet-list clearfix">';
		$html.=$htmllist;
		$html.='</div></div>';
		if ($position==__("Bottom", $td) || $position==__("Top and bottom", $td)) {
			$html.=str_replace("gti_letterindex","gti_letterindexbottom",$htmlindex);
		}		
		$html.='</div>';
		return $html;	
}


function getGlossaryHtmlBySourceItem($gid){
	global $td;

	$html="";
	$sourcetax= get_post_meta($gid,"gti_sourcetaxonomy",true);
	$sourceitem = get_post_meta($gid,"gti_sourceitem",true);
	$hideempty=get_post_meta($gid,"gti_hideempty",true);
	$shownumbers=get_post_meta($gid,"gti_shownumbers",true);
	$showsymbols=get_post_meta($gid,"gti_showsymbols",true);
	$showcount=get_post_meta($gid,"gti_showcount",true);
	$showtitle=get_post_meta($gid,"gti_showtitle",true);
	$textbefore= get_post_meta($gid,"gti_textbefore",true);
	$textafter= get_post_meta($gid,"gti_textafter",true);
	$behaviour= get_post_meta($gid,"gti_indexpanelbehaviour",true);
	$position= get_post_meta($gid,"gti_indexpanelposition",true);
	$columncount= get_post_meta($gid,"gti_columncount",true);
	$theme= get_post_meta($gid,"gti_theme",true);
	 $args = "orderby=name&order=asc&".$args;
	 $sourceitemarr=explode(",", $sourceitem);
	 $post_types = 'babynames'; 
$args="";
	if ($hideempty==__('Hide empty',$td)) {
		$args="hide_empty=1";
	} else {
		$args="hide_empty=0";
	}

	$colsize="33%";
	if (is_numeric($columncount)) {
				if($columncount>0){
						$colsize=floor(100/$columncount) . "%";
				}
	}	
	 $args = "orderby=name&order=asc&".$args;
	 $sourceitemarr=explode(",", $sourceitem);
	// $post_types = get_post_types(); 
//	 print_r($post_types);
	
	/* 	 
	 if ($sourceitem == "*" && $sourcetax=='category')  {
	 	

	 	 $posts = get_posts(
 	array('numberposts' => -1,
 	 			'orderby' => 'title',
 				'order' => 'asc',
				'post_type' => 'post',
 					)
 				);



		} else { */
?><pre><?php print_r($post_types); ?> </pre><?php
?><pre><?php print_r($sourcetax); ?> </pre><?php
?><pre><?php print_r($sourceitemarr); ?> </pre><?php

 	 $posts = get_posts(
 	array('numberposts' => -1,
			'orderby' => 'title',
			'order' => 'asc',
			'post_type' => $post_types
			)
		);

//}	
 				

	 if (count($posts)==0) return __("No posts found!",$td);
	 
   foreach ( $posts as $post ) {
   mb_internal_encoding(mb_detect_encoding($post->post_title));

	 $firstchar = trim(mb_substr((rmBOM($post->post_title)),0,1));
		
//echo $firstchar . " ----- " . $term->name . " @@@@@ " . uniord($firstchar) . "#" . alphanum($firstchar) . "#" . "<br/>";

   		if (is_numeric($firstchar)) {
   			if($shownumbers==__("Show numbers", $td)){
				 	$letters['#'][]=$post;
				}
			} elseif (!alphanum($firstchar)) {
					if ($showsymbols==__("Show symbols", $td)) {
				 		$letters['@'][]=$post;
				 	}	

	/*		} elseif (!preg_match("/^[a-zA-Z\p{Cyrillic}0-9\s\-]+$/u", $term->name[0])) {
					if ($showsymbols==__("Show symbols", $td)) {
				 		$letters['@'][]=$term;
				 	}	
*/

			} else {
				 $letters[strtoupper($firstchar)][]=$post;
			}
   }
	 $htmllist="";
	 $htmlindex='<div class="gti_clear"></div><div id="gti_letterindex"><ul>';
	 if ($behaviour==__("Filtering", $td)){
			$htmlindex.='<li><a href="#gti_all">'.__('All', $td).'</a></li>';			 			
	 }
	 foreach ($letters as $letter=>$lettervalue) {
	 		$ident=str_replace("#","sym",$letter);
	 		$ident=str_replace("@","num",$ident);
	 	  if ($behaviour==__("Navigation", $td)) {
				$htmlindex.='<li><a href="#gti_'.$ident.'">'.$letter.'</a></li>';	
			} else {
				$htmlindex.='<li><a id="gti_sel_'.$ident.'" href="#">'.$letter.'</a></li>';	
			}
		
			$htmllist.="<div id='gti_div_" . $ident . "'><div class='blockletter' id='gti_" . $letter . "'>".$letter."</div>";
			$htmllist.="<ul>";
				foreach ($lettervalue as $theterm) {
					$htmllist.='<li style="width:'.$colsize.';"><a href="'.get_permalink($theterm->ID).'">'.$textbefore.$theterm->post_title.$textafter.'</a></li>';
			}
			$htmllist.="</ul><div class='gti_clear'></div></div>";
	 	}
		$htmlindex.='</ul><div class="gti_clear"></div></div>';
		if ($showtitle==__("Show Title", $td)) {
			$html.='<h2>'.get_the_title($gid).'</h2>';
		}
		$html.='<div id="gticontainer">';
		if ($position==__("Top", $td) || $position==__("Top and bottom", $td)) {
			$html.=$htmlindex;
		}		
		$html.='<div class="tagindex">';
		$html.=$htmllist;
		$html.='</div>';
		if ($position==__("Bottom", $td) || $position==__("Top and bottom", $td)) {
			$html.=str_replace("gti_letterindex","gti_letterindexbottom",$htmlindex);
		}		
		$html.='</div>';
		return $html;	
}

	
	function showGlossary($atts){
		
		
		extract(shortcode_atts(array(  
        "gid" => '-1',
		"gender" => ""
    ), $atts));
    
    if ($gid!="-1"){
    	
		    $theme= get_post_meta($gid,"gti_theme",true);
				if ($theme=='') $theme='default';
				
				wp_register_style( 'gti-style', plugins_url( 'styles/'.$theme , __FILE__ ) );	
				wp_enqueue_style( 'gti-style' ); 
				
				$sourceitem = get_post_meta($gid,"gti_sourceitem",true);
				if ($sourceitem=='') {
					//echo 'getGlossaryHtmlBySourceTax';
					$output= getGlossaryHtmlBySourceTax($gid,$gender);
				} else {
					echo 'getGlossaryHtmlBySourceItem';
					$output= getGlossaryHtmlBySourceItem($gid);
   			}
  	}


    return $output;
	}
	
	add_shortcode('showglossary', 'showGlossary'); 
	 
	 


 //---------------------------------TINYMCE COMBO-------------------------------------------
if(!class_exists('ShortcodesEditorSelector')):
 
class ShortcodesEditorSelector{
	var $buttonName = 'AZGlossaries';
	
	function addSelector(){
		if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
			return;
 
	    if ( get_user_option('rich_editing') == 'true') {
	      add_filter('mce_external_plugins', array($this, 'registerTmcePlugin'));
	      add_filter('mce_buttons', array($this, 'registerButton'));
	    }
	}
 
	function registerButton($buttons){
		array_push($buttons, "separator", $this->buttonName);
		return $buttons;
	}
 
	function registerTmcePlugin($plugin_array){
		$plugin_array[$this->buttonName] = plugins_url() . '/AZGlossaryIndex/js/tinymcecombo-legacy.js.php';
		//if ( get_user_option('rich_editing') == 'true') 
		// 	var_dump($plugin_array);
		return $plugin_array;
	}

}
 
endif;
 
global $tinymce_version;
 
 
 
if ( version_compare( $tinymce_version, '4018' ) >= 0 ) {
  add_action('init', 'my_mce_button');
} else {

if(!isset($shortcodesES)){
	$shortcodesES = new ShortcodesEditorSelector();
	add_action('admin_head', array($shortcodesES, 'addSelector'));
}


}



 



function register_azbutton( $buttons ) {
   array_push( $buttons, "|", "my_mce_button" );
   return $buttons;
}

function add_azplugin( $plugin_array ) {
   //da sostituire con la url del plugin 
   
   //$plugin_array['my_mce_button'] = plugins_url() . '/AZGlossaryIndex/js/tinymcecombo.js.php';
   return $plugin_array;
}

function my_mce_button() {

   if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') ) {
      return;
   }

   if ( get_user_option('rich_editing') == 'true' ) {
      //add_filter( 'mce_external_plugins', 'add_azplugin' );
      add_filter( 'mce_buttons', 'register_azbutton' );
   }

}




//------------------------FRONT JS---------------------------------------
function my_toggle_script() {
	wp_enqueue_script(
		'gtitoggle',
		plugins_url( '/js/toggle.js' , __FILE__ ),
		array( 'jquery' )
	);
}

add_action( 'wp_enqueue_scripts', 'my_toggle_script' );


function AZGlossary_init() {
 $plugin_dir = basename(dirname(__FILE__));
 load_plugin_textdomain( 'AZGlossaryIndex', false, $plugin_dir . "/lang");
}
add_action('plugins_loaded', 'AZGlossary_init');



function updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['azglossary'] = array(
    0 => '',
    1 => __('Glossary updated.'),
    4 => __('Glossary updated.'),
    6 => __('Glosssary created.' ),
    7 => __('Glossary saved.'),
  );

  return $messages;
}

//Manage custom admin messages
add_filter( 'post_updated_messages', 'updated_messages' );

?>
