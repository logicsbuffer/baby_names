<?php
require_once('../../../../wp-load.php');
	require_once('../../../../wp-admin/includes/admin.php');
	//do_action('admin_init');
 
	if ( ! is_user_logged_in() )
		die('You must be logged in to access this script.');
 
	if(!isset($shortcodesES))
		$shortcodesES = new ShortcodesEditorSelector();
?>
 
(function() {
		tinymce.create('tinymce.plugins.<?php echo $shortcodesES->buttonName; ?>', {
			init : function(ed, url) {
 
		},
 
		createControl : function(n, cm) {
			if(n=='<?php echo $shortcodesES->buttonName; ?>'){
                var mlb = cm.createListBox('<?php echo $shortcodesES->buttonName; ?>List', {
                     title : 'AZ Glossary',
		    
                     onselect : function(v) { //Option value as parameter
                        tinyMCE.activeEditor.selection.setContent('[' + v + ']');
                     }
                });
 
               <?php 
	     	    $posts = new WP_Query(array( 
		   'post_type' => 'azglossary',
   		   'orderby' => 'name',
                   'order' => 'asc',
                   'posts_per_page' => '-1' 
		   )); 
		   
                   while ( $posts->have_posts() ) : $posts->the_post(); ?>
									<?php $titleaz=the_title('','',false); 
									if ($titleaz!='') {
									?>
                		mlb.add('<?php echo $titleaz; ?>', 'showglossary gid="<?php the_ID(); ?>"');
									<?php 
										} else {									
									?>
                		mlb.add('Glossary id: <?php the_ID();?>', 'showglossary gid="<?php the_ID(); ?>"');
								<?php } ?>
		  <?php endwhile; ?>
		  <?php wp_reset_query(); ?>			    
 
                return mlb;
             }
 
             return null;
		},
 
		getInfo : function() {
			return {
				longname : 'AZ Glossary Index',
				author : 'Ivano Di Biasi and Giuseppe Liguori',
				authorurl : 'http://www.seocube.it',
				infourl : 'http://www.seocube.it',
				version : "1.0"
			};
		}
	});
 
	tinymce.PluginManager.add('<?php echo $shortcodesES->buttonName; ?>', tinymce.plugins.<?php echo $shortcodesES->buttonName; ?>);
})();