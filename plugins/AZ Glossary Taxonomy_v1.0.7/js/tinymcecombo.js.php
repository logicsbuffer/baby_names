<?php
require_once('../../../../wp-load.php');
	require_once('../../../../wp-admin/includes/admin.php');
	//do_action('admin_init');
 
	if ( ! is_user_logged_in() )
		die('You must be logged in to access this script.');
 
	if(!isset($shortcodesES))
		$shortcodesES = new ShortcodesEditorSelector();
?>
 
(function() {
     
    tinymce.create('tinymce.plugins.my_mce_button', {
         init : function(editor, url) {
            
             editor.addButton( 'my_mce_button', {
			text: 'AZGlossary',
			icon: false,
			type: 'menubutton',
			menu: [
 <?php 
	     	    $posts = new WP_Query(array( 
		   'post_type' => 'azglossary',
   		   'orderby' => 'name',
                   'order' => 'asc',
                   'posts_per_page' => '-1' 
		   )); 
		   
                   while ( $posts->have_posts() ) : $posts->the_post(); ?>
									<?php $titleaz=the_title('','',false); 
									if ($titleaz!='') {
									?>
									{
										text: '<?php echo str_replace("'", "\'", $titleaz); ?>',
										onclick: function() {
										editor.insertContent('[showglossary gid="<?php the_ID(); ?>"]');
                                        }
										},
									<?php 
										} else {									
									?>
									{
										text: 'Glossary id: <?php the_ID();?>',
										onclick: function() {
										editor.insertContent('[showglossary gid="<?php the_ID(); ?>"]');
                                        }
										},
										
                		
								<?php } ?>
		  <?php endwhile; ?>
		  <?php wp_reset_query(); ?>	
				

			]
		});
         }
    });
        tinymce.PluginManager.add('my_mce_button', tinymce.plugins.my_mce_button);
})();