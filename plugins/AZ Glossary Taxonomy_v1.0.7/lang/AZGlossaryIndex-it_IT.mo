��    :      �  O   �      �  �   �  �   �     2     :     K     O     V  ;   c  ;   �  5   �  2     O   D  �   �     e     |  	   �     �     �     �     �     �  
   �  
   �     		     	     &	     3	     I	  �   ^	  
   
     
     &
     6
     F
     e
  �   u
  Q      
   r  
   }     �     �     �     �     �     �     �     �     
          -     3     7     F     Y  $   g     �     �  �  �  �   �  �   a          '     6     <     E  ;   W  Q   �  I   �  W   /  J   �  �   �     �     �     �     �          "     8     M     ^     n     }     �     �     �     �  �   �     �     �     �     �  .        <  �   D  h        x     �     �     �     �     �     �     �  
   	          '     =     U     b     j     }  
   �  '   �  *   �     �     ,            *       8   4       )   &   %   '                              	       !   .   (                              "   6      5               3       7            $                    +   /      1                     2      
   -      9            0   :   #                    A text added after the term name, ex.: If term is SERVICE NAME you could add " in New York" after SERVICE NAME to obtain "car rental in New York" and so on... A text added before the term name, ex.: If term is INGREDIENT you could add "recipes with " before INGREDIENT to obtain "recipes with pasta" and so on... Add New Add New Glossary All Bottom Column Count Decide whether you want to show terms starting with numbers Decide whether you want to show terms starting with symbols Decide whether you want to show terms with zero posts Decide whether you want to show the Glossary Title Decide whether you want to show the post count for each term. Ex.: Travels (10) Define the Index Panel behaviour. It could be a filtering panel or a navigation panel. In Navigation mode you will see all the terms of each letter, in filter mode you will see the terms of one letter a time. Display Glossary Title Edit Glossary Filtering Glossary Index Options Glossary not found Glossary saved. Glossary updated. Glosssary created. Hide Title Hide empty Hide numbers Hide post count Hide symbols Index Panel Behaviour Index Panel Position Insert the specific category or term. The plugin display posts inside. Insert terms id comma separated ex.: 4,84,12. Anyway you must select a source taxonomy (mandatory field) Navigation New Glossary No posts found! No terms found! Number of columns for each row Search Glossary Select the Glossary look and feel<br/>If you would customize your glossary, you need to create a new css file named as you like and save it in the plugin "styles" folder. Select the source taxonomy (category or custom taxonomy) to get the desired items Show Title Show empty Show numbers Show post count Show posts count Show symbols Source Taxonomy Specific Term id Specific Terms id Terms with no posts Text after terms Text before terms Theme Top Top and bottom Use this shortcode View Glossary Where you want the panel to be shown in a page or article or use into your wordpress theme. Project-Id-Version: AzGlossaryIndex
POT-Creation-Date: 2013-05-16 13:02+0100
PO-Revision-Date: 2013-05-16 13:05+0100
Last-Translator: Seo Cube s.r.l. <info@seocube.it>
Language-Team: SeoCube <info@seocube.ti>
Language: Italiano
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.5
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Un testo aggiunto dopo il termine. Es. Se il termine è una tipologia di servizio potresti aggiungere "New York" dopo il termine per ottenere elementi tipo "Noleggio auto a New York" e cosi via Un testo aggiunto prima del termine. Es. Se il termine è un ingrediente potresti aggiungere "Ricette con" prima del termine per ottenere elementi tipo "Ricette con pasta" e cosi via Aggiungi nuovo Aggiungi nuovo Tutti In basso Numero di colonne Scegli se visualizzare i termini che iniziano con un numero Scegli se visualizzare i termini che iniziano con un simbolo o carattere speciale Scegli se visualizzare o meno i termini a cui non è associato alcun post Scegli se visualizzare il titolo del glossario all'interno della pagina di destinazione Scegli se mostrare il conteggio dei post per ogni termine. Es. Viaggi (10) Scegli se l'indice del pannello si comporta come un filtro o come un collegamento ancora alle lettere. Nella modalità filtro saranno visibili solo i termini della lettera selezionata, mentre in modalità Navigazione saranno mostrate tutte le lettere Mostra titolo glossario Modifica Filtro Opzioni Glossary Index Glossario non trovato Glossario aggiornato. Glossario aggiornato Glossario creato Nascondi titolo Nascondi vuoti Nascondi i numeri Nascondi conteggio post Nascondi i simboli Comportamento indice Posizione indice A-Z Inserisci la categoria o il termine specifico. Il plugin mostrerà i post all'interno. Inserisci l'id del termine separato da virgola (es. 4,8,12). In ogni caso è obbligatorio selezionare una tassonomia sorgente. (campo facoltativo) Navigazione Nuovo Nessun post trovato! Nessun termine trovato! Numero di colonne in cui organizzare i termini Ricerca Seleziona l'aspetto grafico del glossario<br/>Se vuoi utilizzare un tuo style css personalizzato, carica un file css nella cartella "styles" del plugin e comparirà automaticamente nella lista dei temi. Seleziona la tassonomia sorgente (categoria o tassonomia custom) da cui ottenere gli elementi desiderati Mostra titolo Mostra vuoti Mostra i numeri Mostra conteggio post Mostra conteggio post Mostra i simboli Tassonomia sorgente ID Termine specifico ID Termine Termini senza post Testo dopo il termine Testo prima del termine Tema grafico In alto In alto e in basso Usa questo shortcode Visualizza Scegli dove mostrare il pannello indice in una pagina o in un articolo oppure usa  nel tuo tema wordpress. 