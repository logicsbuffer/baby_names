<?php
/*
Plugin Name: Baby Names Tabs

*/


function width_get_meta( $value ) {
	global $post;
	$field = get_post_meta( $post->ID, $value, true );
	if ( ! empty( $field ) ) {
		return is_array( $field ) ? stripslashes_deep( $field ) : stripslashes( wp_kses_decode_entities( $field ) );
	} else {
		return false;
	}
}
function width_add_meta_box() {
	add_meta_box(
		'width-width',
		__( 'Product1', 'width' ),
		'width_html',
		'babynames',
		'normal',
		'default'
	);
	add_meta_box(
		'width-width',
		__( 'IMDB', 'width' ),
		'width_html',
		'babynames',
		'normal',
		'default'
	);
}
add_action( 'add_meta_boxes', 'width_add_meta_box' );
function width_html( $post) {
	wp_nonce_field( '_width_nonce', 'width_nonce' ); ?>
	
	<style>	
	
	div#sources_1 {
		display: grid;
	}		
	div#sources_2 {
		display: grid;
	}		
	div#sources_3 {
		display: grid;
	}
	div#sources_4 {
		display: grid;
	}
	textarea#width_source1 {
		height: 70px;
	}
	textarea#width_source2 {
		height: 70px;
	}
	textarea#width_source3 {
		height: 70px;
	}	
	textarea#width_source4 {
		height: 70px;
	}	
	</style>	

	
	<div id="sources">
		
		<div id="sources_1">	
			<label for="width_source1"><?php _e( 'Name Info', 'width' ); ?></label>		
			<textarea rows="10" cols="10" type="text" name="width_source1" id="width_source1" ><?php echo width_get_meta( 'width_source1' ); ?> </textarea>
		</div>				
		
		<div style="display: none;" id="sources_2">
			<label for="width_source2"><?php _e( 'Similar Names', 'width' ); ?></label>		
			<textarea rows="10" cols="10" type="text" name="width_source2" id="width_source2" ><?php echo width_get_meta( 'width_source2' ); ?> </textarea>
		</div>
		
		<!-- <div id="sources_3">
			<label for="width_source3"><?php _e( 'Popularity', 'width' ); ?></label>		
			<textarea rows="10" cols="10" type="text" name="width_source3" id="width_source3" ><?php echo width_get_meta( 'width_source3'); ?></textarea>
		</div> -->

		<div id="sources_4" style="display:none;">
			<label for="width_source4"><?php _e( 'Comments', 'width' ); ?></label>		
			<textarea rows="10" cols="10" type="text" name="width_source4" id="width_source4" ><?php echo width_get_meta( 'width_source4'); ?></textarea>
		</div>
	</div>
		
	
	
	
	<?php
}
add_action( 'save_post', 'width_save' );

function width_save( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  return; 
		if ( ! isset( $_POST['width_nonce'] ) || ! wp_verify_nonce( $_POST['width_nonce'], '_width_nonce' ) ) return; 
		if ( ! current_user_can( 'edit_post', $post_id ) ) return; 
 	
		if ( isset( $_POST['width_source1'] ) ){
		update_post_meta( $post_id, 'width_source1', esc_attr( $_POST['width_source1'] ) );		
		}
		if ( isset( $_POST['width_source2'] ) ){
		update_post_meta( $post_id, 'width_source2', esc_attr( $_POST['width_source2'] ) );		
		}
		if ( isset( $_POST['width_source3'] ) ){
		update_post_meta( $post_id, 'width_source3', esc_attr( $_POST['width_source3'] ) );		
		}
		if ( isset( $_POST['width_source4'] ) ){
		update_post_meta( $post_id, 'width_source4', esc_attr( $_POST['width_source4'] ) );		
		}
		
		//Check your nonce!

		//If calling wp_update_post, unhook this function so it doesn't loop infinitely
		remove_action('save_post', 'width_save');
					
					
		$firstparagraph = width_get_meta( 'width_firstp' ); 
		$source1 = width_get_meta( 'width_source1' ); 
		$source2 = width_get_meta( 'width_source2' ); 
		$source3 = width_get_meta( 'width_source3' ); 			
		$source4 = width_get_meta( 'width_source4' ); 			
		
		ob_start();
	
		// Create and Print out the HTMl
		echo "<div class='movie-info-main' style='width:100%;float:left;'>";
		
		//First Paragraph and sources
		
		$source1_trim = trim($source1);
		$source2_trim = trim($source2);
		$source3_trim = trim($source3);
		$source4_trim = trim($source4);
		
		//if (!empty($source1_trim)) {
		echo  "<p style='text-align: center;'>[wptab name='Name Info']";
		echo html_entity_decode($source1_trim);
		echo "[/wptab]</p>";
		//}
		
		//if (!empty($source2_trim)) {
		echo  "<p style='text-align: center;'>[wptab name='Similar Names']";
		//echo html_entity_decode($source2_trim);
		echo "<h2 style='text-align:center;''>Similar Names</h2>";
		echo '[related_posts_by_tax posts_per_page="10"]';
		echo "[/wptab]</p>";


		//}				
		//if (!empty($source3_trim)) {
		//echo  "<p style='text-align: center;'>[wptab name='Popularity']";		
		//echo html_entity_decode($source3_trim);
		//echo "[/wptab]</p>";
		//}
		//if (!empty($source4_trim)) {
		echo  "<p style='text-align: center;'>[wptab name='Comments']";		
		echo "[wpsites_comment_form][/wptab][end_wptabset]</p>";
		//}		
		
		echo "</div>";
		
		echo "[crp]";

		//End main div																	
		echo  "</div>";		


		$output_movie = ob_get_contents();		
		ob_end_clean();
 		
		wp_update_post(
		array(
			'ID' => $post_id, 			 
			'post_content' => $output_movie)
		); 
			
		// re-hook this function
		add_action('save_post', 'width_save');
		
		}



		add_shortcode( 'wpsites_comment_form', 'wpsites_comment_form_shortcode' );
		function wpsites_comment_form_shortcode() {
		    ob_start();
		    comments_template();
		    $cform = ob_get_contents();
		    ob_end_clean();
		    return $cform;
		 }
