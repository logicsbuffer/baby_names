<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>
	
	Found <?php echo $query->found_posts; ?> Results<br />
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Next', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Previous' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<ul class="three-column grid results-grid mobile-ad-after">
	<?php

	while ($query->have_posts())
	{
		$query->the_post();
		//$post_obj = $query->the_post();
		$post_id = get_the_ID();
		$term_obj_list = get_the_terms( $post_id, 'baby_gender' );
		$terms_slug = join(', ', wp_list_pluck($term_obj_list, 'slug'));
		$terms_name = join(', ', wp_list_pluck($term_obj_list, 'name'));
		
		?>
	<li class="similar-name-result grid__cell unit-6-12--med unit-4-12--wideload">
            <h4><a target="_top" class="name_link <?php echo $terms_slug; ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <p class="results-meta">
              <span class="meta-section"> <span class="meta-title">Gender: </span><span class="meta-sex <?php echo $terms_slug; ?>"><?php echo $terms_name; ?></span> </span>
            </p>
          </li>	
		<?php
	}
	?>
	</ul>
	
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />

	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Next', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Previous' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<?php
}
else
{
	echo "No Results Found";
}
?>