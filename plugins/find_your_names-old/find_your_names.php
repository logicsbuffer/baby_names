<?php
/*
Plugin Name: Find Your Name
Description: Find Your Baby Name
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'find_your_names_script_front_css');
add_action('wp_enqueue_scripts', 'find_your_names_script_front_js');
add_action('admin_enqueue_scripts', 'find_your_names_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
add_shortcode( 'show_list_baby_name', 'list_baby_name' );
add_shortcode( 'show_baby_names_list', 'baby_names_list' );
add_shortcode( 'show_baby_names_list_popular', 'baby_names_list_popular' );

}
function baby_names_list_popular( $atts ) {
    ob_start();

    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'babynames',
        'order' => 'date',
        'orderby' => 'title',
        'baby_category' => '',
        'name_popularity' => 'popular',
        'baby_gender' => '',
        'origin' => '',
        'posts_per_page' => -1,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
		'post_type' => $type,
        'baby_category' => $baby_category,
        'baby_gender' => $baby_gender,
        'origin' => $origin,
        'name_popularity' => $name_popularity,
        'posts_per_page' => $posts_per_page,
        'order' => 'ASC',
        'orderby' => 'date',
    );
    $query = new WP_Query( $options );
    // run the loop based on the query
    if ( $query->have_posts() ) { ?>
        <div id="hp-popular-names">
        <div id="hp-cloud">
            <?php while ( $query->have_posts() ) : $query->the_post(); 
			$baby_gender = get_the_terms( $post->ID,'baby_gender');
			$gender_class = $baby_gender[0]->slug;
			?>
			 <span class="popular_names_list"><a target="_top" class="name_link <?php echo $gender_class; ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
	
            <?php endwhile;
            wp_reset_postdata(); ?>
        </div>
        </div>
    <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }
}

function baby_names_list( $atts ) {
    ob_start();

    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'babynames',
        'order' => 'date',
        'orderby' => 'title',
        'baby_category' => '',
        'name_popularity' => '',
        'baby_gender' => '',
        'origin' => '',
        'posts_per_page' => -1,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
		'post_type' => $type,
        'baby_category' => $baby_category,
        'baby_gender' => $baby_gender,
        'origin' => $origin,
        'name_popularity' => $name_popularity,
        'posts_per_page' => $posts_per_page,
        'order' => 'ASC',
        'orderby' => 'title',
    );
    $query = new WP_Query( $options );
    // run the loop based on the query
    if ( $query->have_posts() ) { ?>
        <div class="results-name-examples">
            <?php while ( $query->have_posts() ) : $query->the_post(); 
			$baby_gender = get_the_terms( $post->ID,'baby_gender');
			$gender_class = $baby_gender[0]->slug;
			?>
			<span class="name-preview "><a target="_top" class="name_link <?php echo $gender_class; ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </div>
    <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }
}

function list_baby_name( $atts ) {
    ob_start();

    // define attributes and their defaults
    extract( shortcode_atts( array (
        'type' => 'babynames',
        'order' => 'date',
        'orderby' => 'title',
        'baby_category' => '',
        'name_popularity' => '',
        'baby_gender' => '',
        'origin' => '',
        'posts_per_page' => -1,
    ), $atts ) );
 
    // define query parameters based on attributes
    $options = array(
		'post_type' => $type,
        'baby_category' => $baby_category,
        'baby_gender' => $baby_gender,
        'origin' => $origin,
        'name_popularity' => $name_popularity,
        'posts_per_page' => $posts_per_page,
        'order' => 'ASC',
        'orderby' => 'title',
    );
/*   $options1 = array(
		'post_type' => $type,
        'origin' => $origin,
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'title',
    ); */
    $query = new WP_Query( $options );
    // run the loop based on the query
    if ( $query->have_posts() ) { ?>
         <ul class="names-listing">
            <?php while ( $query->have_posts() ) : $query->the_post(); 
			$baby_category = get_the_terms( $post->ID,'baby_gender'); ?>
			
            <li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </ul>
    <?php
        $myvariable = ob_get_clean();
        return $myvariable;
    }
}
function find_your_names_script_back_css() {

}

function find_your_names_script_front_css() {
		/* CSS */
        wp_register_style('find_your_names_style', plugins_url('css/find_your_names.css',__FILE__));
        wp_enqueue_style('find_your_names_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function find_your_names_script_back_js() {
	
}



function find_your_names_script_front_js() {
 	
			   
        wp_register_script('find_your_names_script', plugins_url('js/find_your_names.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('find_your_names_script');

}
