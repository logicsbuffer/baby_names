<?php
/**
 * Template Name: Home page1

 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package i-excel
 * @since i-excel 1.0
 */

get_header();
?>
<link
rel='stylesheet' id='i-excel-homestyle-css'  href='https://largebannerprinting.com/wp-content/themes/i-excel/css/home-style.css' type='text/css' media='all' />
<?php $banner_image = get_field('banner_image');
if($banner_image){
	$banner_image = get_field('banner_image');
}else{
$banner_image='https://largebannerprinting.com/wp-content/uploads/2017/06/home-page-banner.jpg';
}
?>
<img class="main_banner" src="<?php echo $banner_image;?>" alt="Large Banner Printing">
<div class="banner_image">
<img class="title_left" src="https://largebannerprinting.com/wp-content/uploads/2017/06/heading-leftside.png">
<div class="banner_col"><span class="banner_title">Large Banner</span><span class="banner_printing"> Printing</span></div>
<img class="title_right" src="https://largebannerprinting.com/wp-content/uploads/2017/06/heading-rightside.png">
</div>
<div class="custom_dis_table">
<div class="center_content">
<div class="hidden-xs tab_container">
<div class="buy_col"><span class="buy_span">Large Banner Printing </span></div>
<div class="st_pricing buy_col"><span class="buy_offer"><span class="buy_span">Buy</span><span class="free_span">2</span><span class="buy_span">get</span><span class="free_span">1</span><span class="buy_span">free</span></span><span class="pricing_span"> Pricing</span><span class="pricing_span"> Table</span></div>
<div class="delivery_option_tab">
<a class="delivery_option_active standdelhom" href="#"><span class="delivery_sprite del_standdelhom"></span>Standard Prices</a>
<a target="_blank" class="delivery_option_not_active standdel72hom" href="https://largebannerprinting.com/72-hours-delivery/"><span class="delivery_sprite del_standdel72hom"></span>72HR Prices</a>
<a target="_blank" class="delivery_option_not_active standdel48hom" href="https://largebannerprinting.com/48-hours-delivery/"><span class="delivery_sprite del_standdel48hom"></span>48HR Prices </a>
<a target="_blank" class="delivery_option_not_active standdel24hom" href="https://largebannerprinting.com/24-hours-delivery/"><span class="delivery_sprite del_standdel24hom"></span>24HR Prices </a>
</div>
<iframe class="iframe_tabel" scrolling="no" src="https://largebannerprinting.com/product_tabelss.html"> 
</iframe> 
</div>
<div class="visible-xs custom_images">
<img class="" src="https://largebannerprinting.com/wp-content/uploads/2017/07/buy2-get-3rd-free.png">
<img class="" src="https://largebannerprinting.com/wp-content/uploads/2017/07/no-additional-image.jpg">
</div>
<div class="custom_calculator">
<div class="buy_col"><span class="pricing_span">Pricing</span><span class="pricing_calc"> Calculator</span></div>
<?php
echo do_shortcode('[show_custom_fancy_product]');
?>
<div class="hidden-xs custom_p">
<div class="del_options cc_light">
<span class="delivery_sprite del_standdelhom cc_icon"></span><span>Standard Delivery 5-7 working days</span>
</div>
<div class="del_options cc_dark">
<span class="delivery_sprite del_standdel72hom cc_icon"></span><span>72Hrs Delivery 3 working days</span>
</div>
<div class="del_options cc_light">
<span class="delivery_sprite del_standdel48hom cc_icon"></span><span>48Hrs Delivery 2 working days</span>
</div>
<div class="del_options cc_dark">
<span class="delivery_sprite del_standdel24hom cc_icon"></span><span>24Hrs Next day Delivery</span>
</div>
</div>
</div>
</div>
</div>
<?php echo get_template_part('home_static_content');?>
<div id="editor_content_home" class="center_content row">
	<?php while ( have_posts() ): the_post();
		the_content();
	endwhile;
	?>
</div>
<?php
get_footer();
?>