<?php
/**
 * @package   Gantry 5 Theme
 * @author    RocketTheme http://www.rockettheme.com
 * @copyright Copyright (C) 2007 - 2017 RocketTheme, LLC
 * @license   GNU/GPLv2 and later
 *
 * http://www.gnu.org/licenses/gpl-2.0.html
 */

defined('ABSPATH') or die;

use Timber\Timber;

/*
 * Search results page
 */

$gantry = Gantry\Framework\Gantry::instance();
$theme  = $gantry['theme'];

// We need to render contents of <head> before plugin content gets added.
$context              = Timber::get_context();
$context['page_head'] = $theme->render('partials/page_head.html.twig', $context);

$context['title'] = __('Search results for:', 'g5_hydrogen') . ' ' . get_search_query();
//$context['posts'] = Timber::get_posts();

$templates = ['search.html.twig'];


$args1 = array(
        'numberposts' => -1,
        'post_type'   => 'babynames',
        'posts_per_page'   => 25,
        'paged' => $paged
    );


$context['banners_top'] = Timber::get_posts($args1);




Timber::render($templates, $context);
